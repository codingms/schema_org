<?php
$EM_CONF[$_EXTKEY] = array(
	'title' => 'Schema.org',
	'description' => 'Adds Schema.org information into your site.',
	'category' => 'plugin',
	'author' => 'Thomas Deuling',
	'author_email' => 'typo3@coding.ms',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => '0',
	'createDirs' => '',
	'clearCacheOnLoad' => 0,
	'version' => '1.1.0.DEV',
	'constraints' => array(
		'depends' => array(
            'typo3' => '7.6.0-9.5.99',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);
